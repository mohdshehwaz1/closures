
let counterFactory=(function() {

    let counter =0;

    function increment_counter(x) {
        counter=counter+x;
    }

    function decrement_counter(x) {
        counter= counter - x;
    }

    return res ={
        increment : function() {
            increment_counter(1);
        },
        decrement : function() {
            decrement_counter(1);
        },
        value: function(){

            return counter;
        }
        
    }
    return res;


}) ();


module.exports = counterFactory;
